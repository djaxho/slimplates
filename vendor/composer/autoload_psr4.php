<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Media32\\Slim\\View\\' => array($vendorDir . '/media32/slim-view-plates/src'),
    'League\\Plates\\' => array($vendorDir . '/league/plates/src'),
);
