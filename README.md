# Slimplates

This library provides a custom view for the [Slim Framework](http://www.slimframework.com/)
to support the [Plates](http://platesphp.com/) template engine.
This repo was created so I could suss out the qualities of using the [Slim Framework](http://www.slimframework.com/) with the [Plates](http://platesphp.com/) template engine. I've included [Foundation 5](http://foundation.zurb.com/), jquery, and a CDN served version of [Font Awesome](http://fortawesome.github.io/Font-Awesome/icons/). It also uses a cool library called [slim-view-plates](https://github.com/media32/slim-view-plates) to support the Plates template engine with Slim. This layout is just a basic Foundation 5 layout. 

## Usage
__First install [Composer](https://getcomposer.org/) __

Clone this repo and run <code>composer install</code>